const express = require('express');



const app = express();

app.use(express.json());

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]

//  [SECTION] Routes and Controllers for USERS

app.get("/users", (req,res) => {
	return res.send(users);
});

app.post("/users", (req, res) => {
	// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
	// hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter AGE"
		})
	}
})



// Activity:

let artists = [
	{
		name: "Jackie Chan",
		songs: ["Who am I?", "The Medallion"],
		album: "Kungfu kid",
		isActive: true
	},
	{
		name: "Bruce Lee",
		songs: ["Enter the Dragon", "Fist of Fury"],
		album: "Drunken Master",
		isActive: true
	},
	{
		name: "Manny Pacquiao",
		songs: ["Para Sayo Ang Laban Nato", "Bilog"],
		album: "Fight Night",
		isActive: true
	}
]

app.get("/artists", (req, res) => {
    return res.send(artists);
});


app.post("/artists", (req, res) => {

    if(!req.body.hasOwnProperty("name")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })

    }
if(!req.body.hasOwnProperty("songs")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter SONGS"
        })

    }

    if(!req.body.hasOwnProperty("album")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter ALBUM"
        })

    }
if(!req.body.hasOwnProperty("isActive")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter ISACTIVE"
        })

    }

    if(req.body.isActive !== true) {
        return res.status(400).send({
            error: "Bad Request - user isActive status is false"
        })

    }
});






const port = 4000
		
app.listen(port , () => {
	console.log(`API is now online on port ${port}`);
})