const chai = require("chai");
const { assert } = require("chai");

// import and use chai-http to allow chai to send request to our server

const http = require("chai-http");
chai.use(http);

describe("API Test Suite for our users", () => {

	it("Test API get users is running", (done) => {
		// request() method is used from chai to create an http request given vto the server
		// get("endpoint") method is used to run/ access a get method route
		// end() method is used to access the response from the route. It has anpnymous functions as an argument that recieves 2 objects, the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// isDefined is assertion that given data is not undefined. It's like a shortcut to .notEqual(typeof data, undefined)
			// assert.isDefined(res);
			assert.notEqual(typeof res.body[0], 'undefined')
			// done() method is used to tell chai-http when the test is done
			done();
		})
	})

	it("Test API get users returns an array", (done) => {
		// request() method is used from chai to create an http request given vto the server
		// get("endpoint") method is used to run/ access a get method route
		// end() method is used to access the response from the route. It has anpnymous functions as an argument that recieves 2 objects, the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// res.body contains the body of the response. The data sent from res.send()
			// isArray() is an assertion that the given data is an array
			assert.isArray(res.body);
			done();
		})
	})

	it("Test API get users array first object username is Jojo", (done) => {
		// request() method is used from chai to create an http request given vto the server
		// get("endpoint") method is used to run/ access a get method route
		// end() method is used to access the response from the route. It has anpnymous functions as an argument that recieves 2 objects, the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			// res.body contains the body of the response. The data sent from res.send()
			// isArray() is an assertion that the given data is an array
			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})

	it("Test API get users array last object is not undefined", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err,res) => {
			const { body } = res
			assert.notEqual(typeof body[body.length-1], 'undefined')
			done();
		})
	})

	it("Test API post users returns 400 if no name", (done) => {
		// post() which is used by chai http to access a post method route
		// type() which is used to tell chai that request body is going to be stringified as a json
		// send() is used to send the request body
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done()
		})
	})

	// MINI ACTIVITY

	it("Test API post users returns 400 if no age", (done) => {

		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			name: "jin",
			username: "jin92"
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			done()
		})
	})
})


// Activity

describe("API Test Suite for artists", () => {
    it("Test Case 1: get artists method is running", (done) => {
        chai.request("http://localhost:4000")
        .get("/artists")
        .end((err, res) => {
            assert.isDefined(res.body[0]);
            done();
        })
    })


    it("Test Case 2: get artists array first object's songs is an array", (done) => {
        chai.request("http://localhost:4000")
        .get("/artists")
        .end((err, res) => {
            assert.isArray(res.body[0].songs);
            done();
        })
    })

    it("Test Case 3: post artists returns 400 if no name", (done) => {
         chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            //name: "Test Artist 1",
            songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
            album: "Test Artist 1 Album 1",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })
	it("Test Case 4: post artists returns 400 if no songs", (done) => {
         chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Test Artist 1",
            //songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
            album: "Test Artist 1 Album 1",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })

    it("Test Case 5: post artists returns 400 if no album", (done) => {
         chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Test Artist 1",
            songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
            //album: "Test Artist 1 Album 1",
            isActive: true
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })

	it("Test Case 6: post artists returns 400 if isActive status is false", (done) => {
         chai.request("http://localhost:4000")
        .post("/artists")
        .type("json")
        .send({
            name: "Test Artist 1",
            songs: ["Test Artist 1 Song 1", "Test Artist 1 Song 2"],
            album: "Test Artist 1 Album 1",
            isActive: false
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })
})